//Practica final
//gcc nombre.c tiempo.c -o nombre -lpthread -lrt -> para ejecutar el programa en Lorca con FileZilla: entrar a Lorca: ssh sctr01@lorca.umh.es
//crear tiempo.c y tiempo.h -> cp ../timepo/tiempo.* ./jose_miguel (en caso de que este dentro de esa carpeta)


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pthread.h> //concerniente a los threads (mutex...)
#include <unistd.h> //Para que funcione sleep();
#include "tiempo2.h" //libreria creada para ejecutar en tiempo real


//Declaración de prototipos de funciones

void * controldep(void *);
void * llenado(void *);
void * alarma(void *);
void * lanzar(void *);
void * reloj(void *);

//Declaración de threads

pthread_t start, control, llenar, alar, tempo;

//Declaración del objeto atributo

pthread_attr_t attr;

//EJERCICIO 4_Declaracion del mutex

pthread_mutex_t candado;
   

//Declaración de variables globales

int volumen, umbral /*= 10*/, caudal /*= 3*/, caudalvaci /*= 7*/, peligro /*= 12*/;
unsigned long tiempo, tiempoej;

//Definición de funciones

/***********************************/
/***********************************/
/***********************************/

int main()
{
   
    //Inicialización de variables
    tiempo=0;
    
    printf("Introduce el tiempo de ejecución: ");
    scanf("%ld",&tiempoej);
    //tiempoej = tiempoej * 20000000;
    
    printf("Introduce el valor del umbral: ");
    scanf("%d",&umbral);
    
    printf("Introduce el valor del caudal: ");
    scanf("%d",&caudal);
    
    printf("Introduce el valor del caudal de vaciado ");
    scanf("%d",&caudalvaci);
    
    printf("Introduce el valor para la alerta de peligro ");
    scanf("%d",&peligro);
    
    //Inicialización del objeto atributo
    
    pthread_attr_init(&attr);
    
    //Cambio del parámetro contentionscope para crear los threads como threads del núcleo
    
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
    
    //Inicializacion del mutex
    /*4*/ pthread_mutex_init(&candado, NULL);
     
            if (pthread_mutex_init(&candado,NULL)!=0)
                 perror("No puedo inicilizar candado");
    
    //Lectura de datos
    
    printf("Soy main y voy a crear los threads\n");
    
    RESET_TMP(); //reseteamos el primer instante de tiempo
    
    pthread_create(&start, &attr, lanzar, NULL);
    
    printf("\nSoy main, he ejecutado el programa y termino\n");
    
    pthread_exit(NULL);
}

/***********************************/
/***********************************/
/***********************************/


void * lanzar(void *arg)
{
    //Crea los nuevos threads
    pthread_create(&tempo, &attr, reloj, NULL);
   
    pthread_create(&control, &attr, controldep, NULL);
    
    pthread_create(&llenar, &attr, llenado, NULL);
   
    pthread_create(&alar, &attr, alarma, NULL);
    
    pthread_exit(NULL);
}

/*************************************************/

void *reloj (void * arg)
{

    while(tiempo < tiempoej)
    {
        SET_TMP(); //LECTURA DE TIEMPO REAL
        tiempo= SEC_TRANS();
        //printf("Tiempo transucrrido ==> %ld\n", tiempo);
        //tiempo = tiempo + 1;
        
    }

    pthread_exit(NULL);
}

/*************************************************/

void *llenado(void * arg)
{
    
    //Inicio seccion critica
    while(tiempo < tiempoej)
    {
        pthread_mutex_lock(&candado);
        
        volumen = volumen + caudal;
       
        printf("Llenado: Volumen del deposito = %d\n\n", volumen);
        
        pthread_mutex_unlock(&candado);
        
        sleep(1);   //Esperamos un segundo cada vez que llena el deposito
        
    }//Fin seccion critica
    
    pthread_exit(NULL);
}

/*************************************************/

void *controldep(void * arg)
{
    
    //Inicio seccion critica
    while(tiempo < tiempoej)
    {
        pthread_mutex_lock(&candado);
        
        if(volumen > umbral)
        {
            volumen = volumen - caudalvaci;

            printf("Control: he reducido el volumen a = %d\n\n", volumen);
        }
        
        pthread_mutex_unlock(&candado);
        sleep(2);
    } //Fin de la seccion critica
    
    pthread_exit(NULL);
}

/*************************************************/

void *alarma(void * arg)
{
    
    //Inicio seccion critica
    while(tiempo < tiempoej)
    {
         pthread_mutex_lock(&candado);
        if(volumen > peligro)
        {
            printf("¡ALERTA! El volumen del deposito es de %d\n\n",volumen);
        }
        
        pthread_mutex_unlock(&candado);
        sleep(1);
    } //Fin de la seccion critica
    
    
    pthread_exit(NULL);
}
