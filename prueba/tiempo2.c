#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "tiempo2.h"
//#include <sys/time.h> ==> No es necesaria al tener tiempo2.h
#include <sys/resource.h>

// Macros
#define RESET_TMP()		clock_get_time(CLOCK_REALTIME, &TIEMPO_time1)	// marca de tiempo 1
#define SET_TMP()		clock_get_time(CLOCK_REALTIME, &TIEMPO_time2)	// marca de tiempo 2

#define SEC_TRANS()		(TIEMPO_time2.tv_sec - TIEMPO_time1.tv_sec)		// segundos transcurridos
#define NSEC_TRANS()	(TIEMPO_time2.tv_nsec - TIEMPO_time1.tv_nsec)	// nanosegundos transcurridos

#define RESET_TIEMPO_rsc()		getrusage(RUSAGE_SELF, &TIEMPO_rsc1)
#define SET_TIEMPO_rsc()		getrusage(RUSAGE_SELF, &TIEMPO_rsc2)

// suspende el thread durante 'ret' milisegundos (puede despertarse por una se�al)
#define SLEEP_MSEC(ret)	{ TIEMPO_retardo.tv_sec= (long)ret/1000; TIEMPO_retardo.tv_nsec= ((long)ret%1000)*1E6; nanosleep (&TIEMPO_retardo, NULL);	}


//********************************************
//********************************************
//********************************************

//A�ADIMOS PARA CONTROL EN TIEMPO REAL EN MAC

#if defined (_MACH_) && !defined(CLOCK_REALTIME)
//#ifdef DARWIN
#include <sys/time.h>
#include <mach/mach_time.h>
#define CLOCK_REALTIME 0 /* 0x2d4e1588 */
#define CLOCK_MONOTONIC 0 /*0x0*/

///*static inline*/ int clock_get_time(int clock_id /*clk_id*/, struct timespec* ts)
//{
//			struct timeval tv;
//			
//			/*if (clock_id != CLOCK_REALTIME)
//			{
//					errno = EINVAL;
//					return -1;
//			}*/
//			/*
//			if (gettimeofday(&tv, NULL) < 0)
//			{
//					return -1;
//			}
//			*/
//			int rv = gettimeofday(&now, NULL);
//			if (rv) return rv;
//			ts->tv_sec = tv.tv_sec;
//			ts->tv_nsec = tv.tv_usec * 1000;
//			return 0;
//}
#endif 
//#endif //DARWIN

//********************************************
//********************************************
//********************************************


// Prototipos Funciones
void  VER_TMP(char *txt);
void  VER_RSC(char *txt);

// Variables Globales
struct timespec  TIEMPO_time1, TIEMPO_time2;
struct rusage  TIEMPO_rsc1, TIEMPO_rsc2;
struct timespec  TIEMPO_retardo;




// Funciones
void VER_TMP(char *txt)
{
	if (txt!=NULL)
		fprintf(stderr, "%s ", txt);

	fprintf(stderr, "[Tiempo consumido real: %2.9fs]\n",
			SEC_TRANS() + (float) NSEC_TRANS() * 1E-9 );
}


void VER_RSC(char *txt)
 {
	 int page_size;

	if (txt!=NULL)
		fprintf(stderr, "%s\n", txt);
	page_size=getpagesize();

	fprintf(stderr, "-------------------------------------------------------------\n");
	fprintf(stderr, "Tiempo consumido (usuario): %2.6f   segundos.\n",	
			(TIEMPO_rsc2.ru_utime.tv_sec - TIEMPO_rsc1.ru_utime.tv_sec)
		    +(float)(TIEMPO_rsc2.ru_utime.tv_usec - TIEMPO_rsc1.ru_utime.tv_usec)*1E-6);	
	fprintf(stderr, "Tiempo consumido (sistema): %2.6f   segundos.\n",
			(TIEMPO_rsc2.ru_stime.tv_sec - TIEMPO_rsc1.ru_stime.tv_sec)
		    +(float)(TIEMPO_rsc2.ru_stime.tv_usec - TIEMPO_rsc1.ru_stime.tv_usec)*1E-6);	

	fprintf(stderr, "-------------------------------------------------------------\n");
	
	fprintf(stderr, "ru_nvcsw:   %4ld  -> Cambios de Contexto voluntarios.\n",
				TIEMPO_rsc2.ru_nvcsw);
	fprintf(stderr, "ru_nivcsw:  %4ld  -> Cambios de Contexto involuntarios.\n",
				TIEMPO_rsc2.ru_nivcsw);
	fprintf(stderr, "ru_nswap:   %4ld  -> Swaps (cache de memoria del proceso en HD).\n",
				TIEMPO_rsc2.ru_nswap);
	fprintf(stderr, "-------------------------------------------------------------\n");

	fprintf(stderr, "ru_inblock: %4ld  -> Operaciones de entrada bloqueadas.\n",
				TIEMPO_rsc2.ru_inblock);
	fprintf(stderr, "ru_oublock: %4ld  -> Operaciones de salida bloqueadas.\n",
				TIEMPO_rsc2.ru_oublock);
	fprintf(stderr, "ru_msgsnd:  %4ld  -> Mensajes enviados.\n",
				TIEMPO_rsc2.ru_msgsnd);
	fprintf(stderr, "ru_msgrcv:  %4ld  -> Mensajes recibidos.\n",
				TIEMPO_rsc2.ru_msgrcv);
	fprintf(stderr, "ru_nsignals:%4ld  -> Senales recibidas.\n",
				TIEMPO_rsc2.ru_nsignals);
	fprintf(stderr, "-------------------------------------------------------------\n");

}
