/*-------------------------------------------------------------
	Cabecera para la medida de tiempo de forma precisa 
		tiempo.h
  -------------------------------------------------------------*/

#ifndef __TIEMPO_H__
#define __TIEMPO_H__

#define _MACH_

#include <stdlib.h>
#include <stdio.h>
#include "tiempo2.h"
#include <sys/time.h>
#include <sys/resource.h>
#include <Errno.h>

//A�ADIMOS PARA CONTROL EN TIEMPO REAL EN MAC

#if defined (_MACH_) && !defined(CLOCK_REALTIME)
//#ifdef DARWIN
#include <sys/time.h>
#include <mach/mach_time.h>
#define CLOCK_REALTIME 0 //0x2d4e1588
#define CLOCK_MONOTONIC 0x0

static inline int clock_get_time(int clock_id, struct timespec* ts)
{
			struct timeval tv;
			
			if (clock_id != CLOCK_REALTIME)
			{
					errno = EINVAL;
					return -1;
			}
			
			if (gettimeofday(&tv, NULL) < 0)
			{
					return -1;
			}
			
			int rv = gettimeofday(&tv, NULL);
			if (rv) return rv;
			ts->tv_sec = tv.tv_sec;
			ts->tv_nsec = tv.tv_usec * 1000;
			return 0;
}
#endif
//#endif //DARWIN

// Macros
#define RESET_TMP()		clock_get_time(CLOCK_REALTIME, &TIEMPO_time1)
#define SET_TMP()		clock_get_time(CLOCK_REALTIME, &TIEMPO_time2)

#define SEC_TRANS()		(TIEMPO_time2.tv_sec - TIEMPO_time1.tv_sec)		// segundos transcurridos
#define NSEC_TRANS()	(TIEMPO_time2.tv_nsec - TIEMPO_time1.tv_nsec)		// nanosegundos transcurridos

#define RESET_RSC()		getrusage(RUSAGE_SELF, &TIEMPO_rsc1)
#define SET_RSC()		getrusage(RUSAGE_SELF, &TIEMPO_rsc2)

// suspende el thread durante 'ret' milisegundos (puede despertarse por una se�al)
#define SLEEP_MSEC(ret)	{ TIEMPO_retardo.tv_sec= (long)ret/1000; TIEMPO_retardo.tv_nsec= ((long)ret%1000)*1E6; nanosleep (&TIEMPO_retardo, NULL);	}

// Prototipos Funciones
void  VER_TMP(char *txt);
void  VER_RSC(char *txt);

// Variables Globales
extern struct timespec  TIEMPO_time1, TIEMPO_time2;
extern struct rusage  TIEMPO_rsc1, TIEMPO_rsc2;
extern struct timespec  TIEMPO_retardo;

#endif

